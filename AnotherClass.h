#pragma once
#include "BaseClass.h"

#include "uuid.h"

class AnotherClass : public BaseClass
{
    Q_OBJECT
public:
    AnotherClass(QObject* parent = nullptr);

    PLANAR_CLASS_UUID(AnotherClass)
};

