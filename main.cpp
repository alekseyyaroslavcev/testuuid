#include "MainWindow.h"
#include <QApplication>
#include <QDebug>

#include "BaseClass.h"
#include "AnotherClass.h"
#include "YetAnotherClass.h"

int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    BaseClass b;
    AnotherClass c;
    YetAnotherClass d;

    qDebug() << _PlanarQuuid<BaseClass>::uuid;
    qDebug() << _PlanarQuuid<AnotherClass>::uuid;
    qDebug() << _PlanarQuuid<YetAnotherClass>::uuid;

    qDebug() << BaseClass::uuid();
    qDebug() << AnotherClass::uuid();
    qDebug() << YetAnotherClass::uuid();

    qDebug() << b.uuid();
    qDebug() << c.uuid();
    qDebug() << d.uuid();

    return a.exec();
}
