#pragma once

#include <QObject>

#include "uuid.h"

class BaseClass : public QObject
{
    Q_OBJECT

public:
    explicit BaseClass(QObject* parent = nullptr);

    PLANAR_CLASS_UUID(BaseClass)
};


