#pragma once
#include <QUuid>

#define PLANAR_UUID "{5986d27c-e7dc-49b8-ad8a-02fa8ba3bcf7}"
#define PLANAR_GENUUID(Class) template<> QUuid _PlanarQuuid<Class>::uuid=QUuid::createUuidV5(QUuid(PLANAR_UUID), QString(#Class));
#define PLANAR_CLASS_UUID(Class) static QUuid uuid() { return _PlanarQuuid<Class>::uuid; }

template<class T>
struct _PlanarQuuid {
    static QUuid uuid;
};
